#!/usr/bin/env python

# Libraries
import json
from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np

# Specify to use CPU, not GPU.
jax.config.update('jax_platform_name', 'cpu')

def Identity(x):
    return x

class ScaleShift(eqx.Module):
    dim: int
    scale: jnp.array
    shift: jnp.array

    def __init__(self, D, scale, shift):
        self.dim = D
        self.scale = jnp.array(scale)
        self.shift = jnp.array(shift) 

    # scale O(1) distribution back to original distribution
    def __call__(self, x):
        return (x * self.scale) + self.shift, jnp.sum(jnp.log(self.scale))

    # normalize distribution to 0 average, 1 standard deviation
    def inv(self, x):
        return (x - self.shift) / self.scale, -jnp.sum(jnp.log(self.scale))

# Class linear transformation with arbitrary initialization of weight and bias
class Linear(eqx.Module):
    weight: jnp.array
    bias: jnp.array

    def __init__(self, in_size, out_size, initweight, initbias):
        shape_w = np.shape(initweight)
        shape_b = np.shape(initbias)
        if shape_w[0] != out_size or shape_w[1] != in_size or shape_b[0] != out_size:
            print('Input weight of bias does not match the dimention of the model')
            exit()
        self.weight = initweight
        self.bias = initbias

    def __call__(self, x):
        return self.weight @ x + self.bias

class MLP(eqx.Module):
    in_size: int
    out_size: int
    depth: int
    width: int
    activation: Callable 
    final_activation: Callable
    layers: list

    def __init__(self, ind, outd, depth, width, actf, final_actf, key):
        self.in_size = ind
        self.out_size = outd
        self.depth = depth
        self.width = width
        self.activation = actf
        self.final_activation = final_actf
        W = ind * width
        keys = jax.random.split(key, depth)
        self.layers = []
        if depth == 0:
            self.layers.append(Linear(ind, outd, jnp.zeros((outd, ind)), jnp.zeros(outd)))
        else:
            self.layers.append(Linear(ind, W, jax.random.normal(keys[0],(W,ind)), jnp.zeros(W)))
            for i in range(1,depth):
                self.layers.append(Linear(W, W, jax.random.normal(keys[i],(W,W)), jnp.zeros(W)))
            self.layers.append(Linear(W, outd, jnp.zeros((outd,W)), jnp.zeros(outd)))

    def __call__(self, x):
        for layer in self.layers[:-1]:
            x = layer(x)/jnp.sqrt(self.width)
            x = self.activation(x)
        x = self.final_activation(x)
        x = self.layers[-1](x)/jnp.sqrt(self.out_size)
        return x

class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: MLP
    trans: Linear

    def __init__(self, key, D, mask=None):
        maskKey, scaleKey = jax.random.split(key)
        if mask is None:
            self.mask = jax.random.randint(maskKey, (D,), 0, 2)
        else:
            self.mask = mask
        self.scale = MLP(D, D, 1, 1, jnp.tanh, Identity, scaleKey)
        self.trans = Linear(D, D, jnp.zeros((D,D)), jnp.zeros(D))

    def map(self, x):
        return self(x)[0]

    # Returns the output and log of Jacobian
    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask) * s)

    # Inverse of the map and its Jacobian
    def inv(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (x-t) * jnp.exp(-s)
        return z, jnp.sum(-(1-self.mask) * s)


class CheckeredAffines(eqx.Module):
    even: AffineCoupling
    odd: AffineCoupling

    def __init__(self, key, D):
        key_even, key_odd = jax.random.split(key)
        mask_even = jnp.arange(D)%2
        mask_odd = 1 - mask_even
        self.even = AffineCoupling(key_even, D, mask=mask_even)
        self.odd = AffineCoupling(key_odd, D, mask=mask_odd)

    def map(self, x):
        return self(x)[0]

    def __call__(self, x):
        x, ld_e = self.even(x)
        x, ld_o = self.odd(x)
        return x, ld_e + ld_o

    # Inverse of the map and its Jacobian
    def inv(self, x):
        x, ld_o = self.odd.inv(x)
        x, ld_e = self.even.inv(x)
        return x, ld_e + ld_o

class RealNVP(eqx.Module):
    dim: int
    depth: int
    layers: list

    def __init__(self, key, D, depth):
        self.dim = D
        self.depth = depth
        keys = jax.random.split(key, depth)
        self.layers = [CheckeredAffines(k, D) for k in keys]

    def map(self, x):
        return self(x)[0]

    def __call__(self, x):
        ld = 0
        for l in self.layers:
            x, lj = l(x)
            ld += lj
        return x, ld

    # Inverse of the map and its Jacobian
    def inv(self, x):
        ld = 0
        for i in range(self.depth):
            x, lj = self.layers[-i-1].inv(x)
            ld += lj
        return x, ld

class RealNVP_SS(eqx.Module):
    ss: ScaleShift
    rnvp: RealNVP

    def __init__(self, key, D, depth, scale=None, shift=None):
        if scale is None:
            scale = jnp.ones((D,))
        if shift is None:
            shift = jnp.zeros((D,))
        self.ss = ScaleShift(D, scale, shift)
        self.rnvp = RealNVP(key, D, depth)

    def __call__(self, x):
        x, ld_rnvp = self.rnvp(x)
        x, ld_ss = self.ss(x)
        return x, ld_ss + ld_rnvp

    def inv(self, x):
        x, ld_ss = self.ss.inv(x)
        x, ld_rnvp = self.rnvp.inv(x)
        return x, ld_ss + ld_rnvp

def make(p, initkey):
    flow = RealNVP_SS(key=initkey, D=p['dim'], depth=p['layers'])
    return flow

def save(filename, hyperparams, flow):
    with open(filename, "wb") as f:
        hyperparams_str = json.dumps(hyperparams)
        f.write((hyperparams_str + "\n").encode())
        eqx.tree_serialise_leaves(f, flow)

def load(filename, initkey):
    with open(filename, "rb") as f:
        hyperparams = json.loads(f.readline().decode())
        flow = make(hyperparams, jax.random.PRNGKey(0))
        return eqx.tree_deserialise_leaves(f, flow), hyperparams

