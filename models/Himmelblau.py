from dataclasses import dataclass
from typing import Tuple
import numpy as np
import jax.numpy as jnp

@dataclass
class Model:
    c0: float
    c1: float
    m: float
    dim = 2

    # returns the log of the distribution
    def dist(self, x):
        return -self.m*(x[0]**2+x[1]-self.c0)**2 - self.m*(x[0]+x[1]**2-self.c1)**2

    def observables(self, x):
        return jnp.array([x[0], x[1], x[0]**2 + x[1]**2])

