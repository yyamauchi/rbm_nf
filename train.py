#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import os.path
import time

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax

from nn import realnvp
import metrics


def train(data, dim, nf, test_data, batch=1000, layers=2, learningrate=1e-3, trainsteps=1e4, seed=None, kl=False, initnf=None, threshold=0, 
          optimzier=optax.adam, print_loss=False, mid_metrics=False, metrics_freq=1, metrics_path='metrics.csv', metrics_selection=[1,1,1,1,1]):
    '''
    Trains a normalizing flow using the specified training settings.

    Args:
        data (str): Path to the training data file.
        dim (int): Dimension of the distribution.
        nf (str): Path to save the trained normalizing flow model.
        test_data (str): Path to a test data file to be used in test loss calulation and the custom function.
        batch (int, optional): Number of samples used per training step. Defaults to 1000.
        layers (int, optional): Number of layers in the flow model. Defaults to 2.
        learningrate (float, optional): Learning rate for the optimizer. Defaults to 1e-3.
        trainsteps (float, optional): Maximum number of training steps. Defaults to 1e4.
        seed (int, optional): Seed for random number generation. Defaults to None.
        kl (bool, optional): Flag indicating whether to use the Kullback-Leibler divergence for loss calculation. Defaults to False.
        initnf (str, optional): Path to an initial normalizing flow. Defaults to None.
        threshold (float, optional): Loss threshold for early stopping. Defaults to 0.
        optimzier (function, optional): Optimizer function. Defaults to optax.adam.
        print_loss (bool, optional): Flag indicating whether to print the loss during training. Defaults to False.
        mid_metrics (bool, optional): If True, calls metrics mid-training. Defaults to False.
        metrics_freq (int, optional): Frequency at which mid-training metrics is called.
            Eg. func_freq=5 will cause the function to be called every fifth training step. Defaults to 1.
        metrics_path (str, optional): Path to save the mid-training metrics to. Defaults to 'metrics.csv'.
        metrics_selection (list, optional): A list of 1s and 0s or True and False to select which metrics to compute.
    '''
    
    # Specify to use CPU, not GPU.
    jax.config.update('jax_platform_name', 'cpu')

    jax.config.update("jax_debug_nans", True)
    jax.config.update("jax_debug_infs", True)

    # Create jax.random keys
    if seed == None:
        seed = time.time_ns()
    sampleKey, initKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    # Read test data from file
    test_xs = []
    test_ps = []
    with open(test_data, 'r') as f:
        for lines in f.readlines():
            r = [np.float64(x) for x in lines.split(',')]
            test_xs.append(r[:-1])
            test_ps.append(r[-1])
    test_xs = jnp.array(test_xs)
    test_ps = jnp.array(test_ps)

    # Read training data from file
    xs = []
    ps = []
    with open(data, 'r') as f:
        for lines in f.readlines():
            r = [np.float64(x) for x in lines.split(',')]
            xs.append(r[:-1])
            ps.append(r[-1])

    xs = jnp.array(xs)
    ps = jnp.array(ps)
    nsamples = len(ps)

    # Normalizing data
    D = xs.shape[1]
    ms = jnp.mean(xs, axis=0)
    ss = jnp.std(xs, axis=0)

    # Create a RealNVP_SS class for the normalizing flow
    if initnf == None:
        flow = realnvp.RealNVP_SS(initKey, dim, layers, ss, ms)
        if print_loss:
            print('Normalizing flow is initialized to be an approximate Gaussian')
    elif os.path.isfile(initnf):
        flow, hyperparams = realnvp.load(initnf, initKey)
        hdim = hyperparams['dim']
        print(f'Normalizing flow is initialized from the file {initnf}')
        if hdim != D or hdim != dim:
            print(f'Error: the dimention of the model ({dim}) or the training data ({D}) does not match the initial NF ({hdim})')
            exit()
        else:
            dim = hyperparams['dim']
            layers = hyperparams['layers']
    else:
        print(f'{initnf} does not exist')
        exit()
    
    # Exit if training settings are bad
    if D != dim:
        print(f'Error: the dimention of the model ({dim}) does not match the training data ({D})')
        exit()

    if nsamples < batch:
        print('Error: the number of samples in training data should be at least as large as the number of samples used per training step')
        exit()

    # print out basic training settings
    if print_loss:
        print(f'----- Training settings -----')
        print(f'Dimension of the distribution is {D}')
        print(f'Real NVP with {layers} layers, followed by a linear scaling & shifting layer')
        print(f'{nsamples} total samples in training data, {batch} samples per training step')
        print(f'ADAM learning rate: {learningrate:.4f}')
        print(f'NF will be trained for {trainsteps} steps or until the loss reduces to {threshold:.2f}')

    # Returns the effective density function after the flow is applied
    @partial(jnp.vectorize, signature='(i)->()', excluded={1})
    def dist_to_gauss(x, flow):
        y, logdet = flow.inv(x)
        return -jnp.sum(y**2)/2 + logdet #2Pi factor in Gaussian is dropped

    # Kullback-Leibler digergence
    def kl_loss(flow, x, p):
        dist = dist_to_gauss(x, flow)
        loss = p - dist
        return jnp.average(loss)

    #Jefferys' divergence
    def j_loss(flow, x, p):
        dist = dist_to_gauss(x, flow)
        kl = jnp.average(p - dist)
        rw = jnp.exp(dist - p)
        rkl = jnp.sum((dist - p)*rw)/jnp.sum(rw)
        return kl + rkl   

    # Compute the loss function and its derivative wrt NN parameteres
    if kl:
        loss_grad = eqx.filter_value_and_grad(kl_loss)
    else:
        loss_grad = eqx.filter_value_and_grad(j_loss)

    # Initialize the optimizer
    opt = optimzier(learningrate)
    opt_state = opt.init(eqx.filter(flow, eqx.is_array))

    @eqx.filter_jit
    def step(flow, opt_state, *, key):
        k, key = jax.random.split(key)
        s = jax.random.choice(k, nsamples, [batch,], replace=False)
        loss, grad = loss_grad(flow, xs[s], ps[s])
        updates, opt_state = opt.update(grad, opt_state)
        flow = eqx.apply_updates(flow, updates)
        return flow, loss, opt_state, key

    hyperparams = {'data': data, 'dim': dim, 'nf': nf, 'ntrain': nsamples, 'layers': layers, 'batch': batch, 
                   'trainsteps': trainsteps, 'learningrate': learningrate, 'seed': seed, 'initnf': initnf, 'threshold': threshold}
    # Train Normalizing flow for the input model
    loss, test_loss, train_step = 1e2, 1e2, 0
    if mid_metrics:
        mid_met_df = metrics.metrics((flow, hyperparams), test_data, csv=None, seed=None, nfsamples=None)
    try:
        start_time = time.time()
        if print_loss:
            print_func = lambda loss, train_step: print(f'{train_step} {loss:.6f} | Test loss: {test_loss:.6f}')
        else:
            print_func = lambda loss, train_step: None
        while (loss > threshold or test_loss > threshold) and train_step < trainsteps:
            flow, loss, opt_state, sampleKey = step(flow, opt_state, key=sampleKey)
            test_loss = j_loss(flow, test_xs, test_ps)
            train_step += 1
            if mid_metrics and train_step % metrics_freq == 0:
                mid_met_df.append(metrics.metrics((flow, hyperparams), test_data, csv=None, seed=None, nfsamples=None))
            print_func(loss, train_step)
    except KeyboardInterrupt:
        pass

    # Update and save Nf and hyperparameters
    trainsteps = train_step
    realnvp.save(nf, hyperparams, flow)
    if mid_metrics:
        mid_met_df.reset_index.to_csv(metrics_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Training normalizing flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
    )
    parser.add_argument('data', type=str, help="training data filename")
    parser.add_argument('dim', type=int, help="the dimension of the distribution")
    parser.add_argument('nf', type=str, help="normalizing flow filename")
    parser.add_argument('test_data', type=str, help="test data filename")
    parser.add_argument('-b', '--batch', type=int, default=1000, help="number of samples used per training step")
    parser.add_argument('-l', '--layers', type=int, default=2, help="number of checkered Affine layers")
    parser.add_argument('-r', '--learningrate', type=float, default=1e-3, help="learning rate for ADAM optimizer")
    parser.add_argument('-s', '--trainsteps', type=int, default=int(1e4), help="number of tarin steps taken before the termination")
    parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
    parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")
    parser.add_argument('--initnf', type=str, default="None", help="NF file to initialize the neural network")
    parser.add_argument('--kl', action='store_true', help="Use KL divergence")
    parser.add_argument('-t', '--threshold', type=float, default= 0, help="loss value to achieve before the termination")
    parser.add_argument('-o', '--optimizer', default=optax.adam, help="training optimizer")
    parser.add_argument('-p', '--print_loss', type=bool, default=True, help="whether to print the loss during training")
    parser.add_argument('--mid_metrics', default=False, help="whether to call metrics mid-training")
    parser.add_argument('-f', '--metrics_freq', type=int, default=1, help="training step frequency of running the additional function")
    parser.add_argument('-y', '--metrics_path', type=str, default='metrics.csv', help="path to save the mid-training metrics to")
    parser.add_argument('-z', '--metrics_selection', type=list, default=[1,1,1,1,1], help="a list of 1s and 0s or True and False to select which metrics to compute")
    args = parser.parse_args()

    if args.seed_time:
        args.seed = None
    train(args.data, args.dim, args.nf, args.test_data, batch=args.batch, layers=args.layers, learningrate=args.learningrate, trainsteps=args.trainsteps, seed=args.seed, kl=args.kl, initnf=args.initnf, 
          threshold=args.threshold, optimizer=args.optimizer, print_loss=True, mid_metrics=args.mid_metrics, metrics_freq=args.metrics_freq, metrics_path='metrics.csv', metrics_selection=args.metrics_selection)