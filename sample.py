#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import time
import jax
import jax.numpy as jnp

from nn import realnvp

def sample(nf, samples, seed=None, Print=False):
    """
    Samples from a train normalizing flow.

    Args:
        nf (str): Path to the trained normalizing flow.
        samples (int): Number of samples to generate.
        seed (int): Seed for the random number generator.
        Print (bool): Flag indicating whether to print the samples or return them.
    """

    # Specify to use CPU, not GPU.
    jax.config.update('jax_platform_name', 'cpu')

    if seed == None:
        seed = time.time_ns()
    samplekey, initkey = jax.random.split(jax.random.PRNGKey(seed), 2)

    flow, hyperparams = realnvp.load(nf, initkey)
    D = hyperparams['dim']

    @partial(jnp.vectorize, signature='(i)->(i)', excluded={1})
    def map(x, flow):
        y, logdet = flow(x)
        return y

    samplekey, pkey = jax.random.split(samplekey)
    x = jax.random.normal(pkey, (samples, D))
    if Print:
        y = map(x, flow)
        for i in range(samples):
            print(*y[i])
    else:
        return map(x, flow)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Sample from normalizing flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('nf', type=str, help="normalizing flow filename")
    parser.add_argument('-S', '--samples', default=1000, type=int, help="number of samples per batch")
    parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
    parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")

    args = parser.parse_args()
    if args.seed_time:
        args.seed = None
    sample(args.nf, args.samples, args.seed, Print=True)



    









