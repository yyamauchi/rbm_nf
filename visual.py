#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import sys
import time

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np
import matplotlib.pyplot as plt
import corner
import pandas as pd

from nn import realnvp

def visual(nf, data, nsamples, filename=None, seed=None, binwidth=.04):
    """
    Samples from a train normalizing flow and generates a corner plot to compare to other data.

    Args:
        nf (str): Path to the trained normalizing flow.
        data (str): Path to the data to compare to.
        nsamples (int): Number of samples to generate.
        filename (str): Name of the file to save the plot to.
        seed (int): Seed for the random number generator.
    """

    # Specify to use CPU, not GPU.
    jax.config.update('jax_platform_name', 'cpu')

    if seed == None:
        seed = time.time_ns()
    samplekey, initkey = jax.random.split(jax.random.PRNGKey(seed), 2)

    # Import NF and its hyperparameters
    flow, hyperparams = realnvp.load(nf, initkey)
    dim = hyperparams['dim']
    ntrain = hyperparams['ntrain']
    depth = hyperparams['layers']
    batch = hyperparams['batch']
    trainsteps = hyperparams['trainsteps']
    initnf = hyperparams['initnf']

    @partial(jnp.vectorize, signature='(i)->(i)')
    def map(x):
        y, logdet = flow(x)
        return y

    @jax.jit
    def sample(key):
        x = jax.random.normal(key, (nsamples, dim))
        y = map(x)
        return y

    start_time = time.time()
    obs = sample(samplekey)
    source = pd.read_csv(data, header=None, sep=',').to_numpy()[:, :-1]
    source_len = len(source)
    source_ratio = nsamples/source_len
    total_time = np.round(time.time() - start_time, 2)

    figure = corner.corner(source,
        weights=np.ones(len(source))*source_ratio,
        labels=[f'x{i}' for i in range(dim)],
        labelpad=0.2,
        bins=[int((max(source[:,i])-min(source[:,i]))//binwidth) for i in range(dim)],
        color='black',
        label_kwargs={"fontsize":30},
        hist_kwargs= {"linewidth":2},
        quantiles=None,
        smooth=(1.7),
        smooth1d=1.0,
        show_titles=True,
        )

    full_figure = corner.corner(np.array(obs), fig=figure, 
        bins=[int((max(obs[:,i])-min(obs[:,i]))/binwidth) for i in range(dim)],
        color='red',
        labels=[f'x{i}' for i in range(dim)],
        labelpad=0.2,
        label_kwargs={"fontsize":30},
        hist_kwargs= {"linewidth":2},
        quantiles=None,
        smooth=(1.7),
        smooth1d=1.0,
        show_titles=True,)

    width = 8
    height = 6
    full_figure.set_size_inches(width, height)

    plt.legend(['Source', 'NF'], fontsize=10, loc='upper right')
    plt.rc('xtick', labelsize=15)    
    plt.rc('ytick', labelsize=15)
    plt.xlabel('\nnfsamples='+str(nsamples)+'\nN='+str(ntrain)+' L='+str(depth)+' B='+str(batch)+' S='+str(trainsteps)+'\nsample time: ' + total_time.__str__(), fontsize=12)
    if filename == None:
        plt.show()   
    else:
        plt.savefig(filename, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Training normalizing flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
        )
    parser.add_argument('nf', type=str, help="normalizing flow filename")
    parser.add_argument('training', type=str, help="training data filename")
    parser.add_argument('-n', '--nsamples', type=int, default=1000, help="number of samples to compare with the training data")
    parser.add_argument('-fn', '--filename', type=str, default='corner.png', help="the filename of the exported figure")
    parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
    parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")
    parser.add_argument('-bw', '--binwidth', type=float, default=.04, help="seed PRNG with current time")
    args = parser.parse_args()
    if args.seed_time:
        args.seed = None
    visual(args.nf, args.training, args.nsamples, args.filename, args.seed, args.binwidth)



