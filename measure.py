#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import pickle
import sys
import time

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np

from models import PG2d, Rosenbrock_banana
from nn import realnvp

# Specify to use CPU, not GPU.
jax.config.update('jax_platform_name', 'cpu')

parser = argparse.ArgumentParser(
        description="Measure expectation values",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
        )
parser.add_argument('model', type=str, help="distribution model filename")
parser.add_argument('nf', type=str, help="normalizing flow filename")
parser.add_argument('-B', '--batches', default=1, type=int, help="number of batches before termination")
parser.add_argument('-S', '--samples', default=10000, type=int, help="number of samples per batch")
parser.add_argument('-R', '--reweight', action='store_true', help="perform reweighting" )
parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")

args = parser.parse_args()

seed = args.seed
if args.seed_time:
    seed = time.time_ns()
samplekey, initkey = jax.random.split(jax.random.PRNGKey(seed), 2)

# Importing model file
with open(args.model, 'rb') as f:
    model = eval(f.read())
Dmodel = model.dim


flow, hyperparams = realnvp.load(args.nf, initkey)
D = hyperparams['dim']

# Error message on dimension of the model and normalizing flow
if D != Dmodel:
        print('Error: the dimention of the model does not match that of the normalizing flow')
        exit()

@partial(jnp.vectorize, signature='(i)->(i),()', excluded={1})
def dist_effective(x, flow):
    y, logdet = flow(x)
    dist = model.dist(y)
    return y, dist + logdet

@jax.jit
def sample(key):
    skey, key = jax.random.split(key)
    x = jax.random.normal(key, (args.samples,model.dim))
    y, dist = dist_effective(x, flow)
    obs = jax.vmap(model.observables)(y)
    rw = jnp.exp(jnp.sum(x**2/2, axis=-1) + dist) * np.power(2*np.pi, model.dim/2)
    norm = np.mean(rw)
    obss = jax.vmap(model.observables)(y)
    if not args.reweight:
        rw = jnp.ones(args.samples)
    obs_rw = jnp.einsum('ij,i->j', obss, rw)
    obs = obs_rw / jnp.sum(rw)
    return norm, *obs

for _ in range(args.batches):
    samplekey, pkey = jax.random.split(samplekey)
    rw, *obs = sample(pkey)
    print(rw, *obs)

    









