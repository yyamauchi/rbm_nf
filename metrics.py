#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import time
import pandas as pd
import jax
import jax.numpy as jnp
import numpy as np
from scipy.stats import kurtosis, skew
import threading

# Create a lock object
lock = threading.Lock()

from nn import realnvp

def metrics(nf, data, csv=None, seed=None, nfsamples=None, metric_selection=[1,1,1,1,1]):
    """
    Computes and stores various metrics during training. 
    This function is meant to evaluate a trained nf.

    Args:
        nf (str): Path to the trained normalizing flow or an array type containing the flow and hyperparameters.
        data (str): Path to the data file to compare nf to.
        csv (str, optional): Path to the csv file to store the metrics. If csv is not provided, the metrics will be returned as a dataframe.
        seed (int, optional): Seed for the random number generator. If None, the seed will be set to the current time.
        nfsamples (int, optional): Number of samples to draw from the nf to estimate the metrics. 
            If None, the number of samples is equal to the size of the data set.
        metric_selection (list, optional): A list of 1s and 0s or True and False to select which metrics to compute.
            Considers the first 4 moments and covariances. Defaults to [1,1,1,1,1]
    """

    if seed == None:
        seed = time.time_ns()
    samplekey, initkey = jax.random.split(jax.random.PRNGKey(seed), 2)

    # Read training data from file
    xs, ps = [], []
    with open(data, 'r') as f:
        for l in f.readlines():
            r = [np.float64(x) for x in l.split(',')]
            xs.append(r[:-1])
            ps.append(r[-1])
    xs, ps = np.array(xs), np.array(ps)
    nsamples = len(ps)

    # Import normalizing flow
    if type(nf) == str:
        try:
            flow, hyperparams = realnvp.load(nf, initkey)
        except:
            print('nf path not found')
            exit(0)
    else:
        try:
            flow, hyperparams = nf
        except:
            print('nf should be either a path or an array type containing two elements: the flow and hyperparameters')
            exit(0)
    D, N, L, B, S = hyperparams['dim'], hyperparams['ntrain'], hyperparams['layers'], hyperparams['batch'], hyperparams['trainsteps']

    # Exit if Dim does not match the training data
    if xs.shape[1] != D:
        print('The dimension of the model does not match the training data')
        exit(0)

    # Seperate xs into 10 subsets
    if nsamples < 10*N:
        xs_subsets = [xs[np.random.choice(nsamples, size=N, replace=True)] for i in range(100)]
        print('The nf was trained on less than 10 times the number of samples given, so bootstrapping will be used to estimate uncertainity')
    else:
        samples = xs[np.random.choice(nsamples, size=N*10, replace=False)]
        xs_subsets = [samples[i:i+N] for i in range(0, len(samples), N)]

    @partial(jnp.vectorize, signature='(i)->(i)', excluded={1})
    def map(x, flow):
        y, logdet = flow(x)
        return y

    @partial(jnp.vectorize, signature='(i)->()', excluded={1})
    def dist_to_gauss(x, flow):
        y, logdet = flow.inv(x)
        return -jnp.sum(y**2)/2 + logdet # 2pi factor in Gaussian is dropped

    def kl_divergence(flow, x, p):
        dist = dist_to_gauss(x, flow)
        kl = p - dist
        return jnp.average(kl)

    def j_divergence(flow, x, p):
        dist = dist_to_gauss(x, flow)
        kl = jnp.average(p - dist)
        rw = jnp.exp(dist - p)
        rkl = jnp.sum((dist - p)*rw)/jnp.sum(rw)
        return kl + rkl

    # Create a dataframe to store metric differences
    try:
        metrics = pd.read_csv(csv)
    except:
        metrics = pd.DataFrame(columns=['N', 'L', 'B', 'S', 'kl', 'j', 'mean', 'var', 'skew', 'kurtosis',  'cov'])
    rowi = len(metrics)

    # Compute divergences
    kl = kl_divergence(flow, xs, ps)
    print(f"Kullback-Leibler divergence: {kl:.5f}")
    j = j_divergence(flow, xs, ps)
    print(f"Jefferys' divergence: {j:.5f}")
    new_row = [N, L, B, S, kl, j, [], [], [], [], []]

    # Compute moments
    if nfsamples == None:
        nfsamples = nsamples
    x = jax.random.normal(samplekey, (nfsamples, D))
    x_nf = map(x, flow)

    if metric_selection[0]:
        for i in range(D):
            mean_subsets = [np.mean(xs_subsets[j][:,i]) for j in range(100)]
            std = np.std(mean_subsets)
            mean_model = np.mean(mean_subsets)
            mean_nf = np.mean(x_nf[:,i])
            diff = float(mean_nf-mean_model)
            print(f"{i}th coodinate's mean: model is {mean_model:.5f} \u00B1 {std:.5f}, normalizing flow is {mean_nf:.5f}.\
                Diff/std ratio is {abs(diff/std):.3f}")
            new_row[6].append(abs(diff/std))

    if metric_selection[1]:
        for i in range(D):
            var_subsets = [np.var(xs_subsets[j][:,i]) for j in range(100)]
            std = np.std(var_subsets)
            var_model = np.var(xs[:,i])
            var_nf = np.var(x_nf[:,i])
            diff = float(var_nf-var_model)
            print(f"{i}th coodinate's variance: model is {var_model:.5f} \u00B1 {std:.5f}, normalizing flow is {var_nf:.5f}.\
                Diff/std ratio is {abs(diff/std):.3f}")
            new_row[7].append(abs(diff/std))

    if metric_selection[2]:
        for i in range(D):
            skew_subsets = [skew(xs_subsets[j][:,i]) for j in range(100)]
            std = np.std(skew_subsets)
            skew_model = skew(xs[:,i])
            skew_nf = skew(x_nf[:,i])
            diff = float(skew_nf-skew_model)
            print(f"{i}th coodinate's skewness: model is {skew_model:.5f} \u00B1 {std:.5f}, normalizing flow is {skew_nf:.5f}.\
                Diff/std ratio is {abs(diff/std):.3f}")
            new_row[8].append(abs(diff/std))

    if metric_selection[3]:
        for i in range(D):
            kurtosis_subsets = [kurtosis(xs_subsets[j][:,i]) for j in range(100)]
            std = np.std(kurtosis_subsets)
            kur_model = kurtosis(xs[:,i])
            kur_nf = kurtosis(x_nf[:,i])
            diff = float(kur_nf-kur_model)
            print(f"{i}th coodinate's kurtosis: model is {kur_model:.5f} \u00B1 {std:.5f}, normalizing flow is {kur_nf:.5f}.\
                Diff/std ratio is {abs(diff/std):.3f}")
            new_row[9].append(abs(diff/std))

    if metric_selection[4]:
        for i in range(D):
            for k in range(i+1, D):
                cov_subsets = [np.cov(xs_subsets[j][:,i], xs_subsets[j][:,k], rowvar=False)[0,1] for j in range(100)]
                std = np.std(cov_subsets)
                cov_model = np.cov(xs[:,i], xs[:,k], rowvar=False)[0,1]
                cov_nf = np.cov(x_nf[:,i], x_nf[:,k], rowvar=False)[0,1]
                diff = float(cov_nf-cov_model)
                print(f"{i}th and {k}th coodinate's covariance: model is {cov_model:.5f} \u00B1 {std:.5f}, normalizing flow is {cov_nf:.5f}.\
                Diff/std ratio is {abs(diff/std):.3f}")
                new_row[10].append(diff/std)


    # Save metrics
    metrics.loc[rowi] = new_row
    if csv == None:
        return metrics
    else:
        metrics.to_csv(csv, index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Training normalizing flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('nf', type=str, help="normalizing flow filename")
    parser.add_argument('data', type=str, help="model samples filename")
    parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
    parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")
    parser.add_argument('-s', '--save', type=str, default=None, help="filename to save metrics to")
    parser.add_argument('-n', '--nfsamples', type=int, default=None, help="number of samples to draw from the nf to estimate the metrics")
    parser.add_argument('-z', '--metric_selection', type=list, default=[1,1,1,1,1], help="a list of 1s and 0s or True and False to select which metrics to compute")
    args = parser.parse_args()

    if args.seed_time:
        args.seed = int(time.time())
    
    metrics(args.nf, args.data, args.save, args.seed, args.nfsamples, args.metric_selection)








