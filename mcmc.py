#!/usr/bin/env python

# Libraries
from functools import partial
import pickle
import sys
from typing import Callable
import time

import jax
import jax.numpy as jnp
import numpy as np
import optax

from models import PG2d, Rosenbrock_banana, student_t, Himmelblau
from mc import metropolis

# Specify to use CPU, not GPU.
jax.config.update('jax_platform_name', 'cpu')

def mcmc(model, n, skip, csv=None, seed=None):

    jax.config.update("jax_debug_nans", True)

    with open(model, 'rb') as f:
        model = eval(f.read())
    D = model.dim

    if seed == None:
        seed = time.time_ns()
    chainKey = jax.random.PRNGKey(seed)

    # TODO hard-coded
    Ntherm = 1000

    # Generate samples via MCMC chain
    chain = metropolis.Chain(model.dist, jnp.zeros(D), chainKey)
    chain.step(N=Ntherm)
    chain.calibrate()

    if csv == None:
        for i in range(n):
            chain.step(N=skip)
            print(*chain.x, model.dist(chain.x))
    else:
        with open(csv, 'w') as f:
            for i in range(n):
                chain.step(N=skip)
                f.write(','.join([str(x) for x in chain.x] + [str(model.dist(chain.x))]) + '\n')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="MCMC sampling",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('-n', '--nsamples', type=int, default=1000, help="total number of samples")
    parser.add_argument('-c', '--skip', type=int, default=1, help="number of samples skipped in MCMC")
    parser.add_argument('-s', '--csv', type=int, default=1, help="file to save data to")
    parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
    parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")
    args = parser.parse_args()

    mcmc(args.model, args.nsamples, args.skip, args.csv, args.seed if not args.seed_time else None)
