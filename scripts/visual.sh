#!/bin/sh  
# run ./scripts/visual.sh

dir='data'
mkdir -p $dir
mkdir -p 'visuals'

t=-10 # Threshold
# n : Number of samples used as training data and sampled from NF
# lr: Learning rate of NN
# l : Number of layers
# b : Training batch size
# s : Number of training steps

for n in 50000; do
    for trial in 1; do
        shuf -n $n 1E6traindata.dat > ${dir}/${n}traindata.dat
        for lr in 1e-4 5e-3 1e-3 5e-2 1e-2; do
            for l in 4; do
                for b in 50000; do
                    for s in 500 1000 1500 2000 2500 3000; do
                        ./nf.py 2 ${dir}/${n}traindata.dat ${dir}/model_N${n}_L${l}_B${b}_S${s}_LR${lr}_T${trial}.pickle -l $l -t -10 -b $b -s $s -r $lr --seed-time
                        ./visual_sample.py models/model_pg2d ${dir}/model_N${n}_L${l}_B${b}_S${s}_LR${lr}_T${trial}.pickle ${dir}/${n}traindata.dat $l -n $n -b $b -s $s -tr $trial -vfn visuals/corner_N${n}_L${l}_B${b}_S${s}_LR${lr}_T${trial}.png --seed-time
                    done
                done
            done
        done
    done
done