#!/bin/sh

model='data/model.pickle'
dir='data/'
l=4
lr=1e-3
c=10
b=100
s=2000

mkdir -p $dir

for n in 100 1000 10000; do
	./nf_inv.py $model ${dir}/nf_l${l}_lr${lr}_n${n}_b${b}_c${c}_s${s}.pickle -l $l -t -10 -n $n -b $n -s $s -c $c --seed-time 
done
