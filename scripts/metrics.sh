#!/bin/sh  
# run ./scripts/metrics.sh

N=50000
L=4
B=50000
S=3000
LR=1e-3
T=1

mkdir -p 'metrics'

for LR in 1e-4 5e-3 1e-3 5e-2 1e-2; do
    for S in 500 1000 1500 2000 2500 3000; do
        model=data/model_N${N}_L${L}_B${B}_S${S}_LR${LR}_T${T}.pickle
        data=data/${N}traindata.dat
        echo "N=${N};L=${L};B=${B};S=${S};LR=${LR};" >> metrics/divergence.dat
        ./metrics.py 2 $model $L $data --seed-time >> metrics/divergence.dat
    done
done