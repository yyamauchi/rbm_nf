#!/bin/sh

model='data/model.pickle'
dir='data/'
l=4
lr=1e-3
b=100
c=10
s=2000
S=10000

for n in 100 1000 10000; do
	./sample.py $model ${dir}/nf_l${l}_lr${lr}_n${n}_b${b}_c${c}_s${s}.pickle $l -B $B -S $S --seed-time 
done
